#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    srand(time(NULL));
    int nombreAleatoire = rand() % 10 + 1;
    int devinette;
    printf("Devinez le nombre entre 1 et 10 : ");
    scanf("%d", &devinette);

    if (devinette == nombreAleatoire) {
        printf("Félicitations ! Vous avez trouvé le nombre %d.\n", nombreAleatoire);
    } else {
        printf("Dommage ! Le nombre était %d.\n", nombreAleatoire);
    }

    return 0;
}
