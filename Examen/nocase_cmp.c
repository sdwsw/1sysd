#include <stdlib.h>
#include <stdio.h>

int nocase_equal(char *s1, char *s2){
	while (*s1 != 0 && *s2 != 0){
		char c1 = (*s1 >= 'A' && *s1 <= 'Z') ? (*s1 + 32) : *s1;// 32 est la différence entre 																						
        	char c2 = (*s2 >= 'A' && *s2 <= 'Z') ? (*s2 + 32) : *s2;//une lettre majuscule et sa 
    		if (c1 != c2) {						//correnspondant en miniscule
            		return 0;
            	}
            	s1++;
        	s2++;
        }
        return *s1 == '\0' && *s2 == '\0';
}


int main() {
    char chaine1[] = "Bonjour";
    char chaine2[] = "BONJOUR";

    if (nocase_equal(chaine1, chaine2)) {
        printf("Les chaînes sont identiques.\n");
    } else {
        printf("Les chaînes sont différentes.\n");
    }
    return 0;
}
