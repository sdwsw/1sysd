
#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>

typedef struct node node;
struct node {
   int val;
   node *next;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    return p;
}

void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) {
        printf("%d ", walk->val);
        fflush(stdout);
        walk = walk->next;
    }
    printf("\n");
}

node *append_val(node *head, int val) {
    node *newnode, *walk;

    newnode = create_node(val);

    if (head == NULL) {
        head = newnode;
    } else {
        walk = head;
        while (walk->next != NULL) {
            walk = walk->next;
        }
        walk->next = newnode;
    }
    return head;
}

node *tab2list(int tab[], int size) {     //Je déclare les trois fonctions
    node *head = NULL;
    for (int i = 0; i < size; i++) {
        head = append_val(head, tab[i]);
    }
    return head;
}

int list2tab(node *head, int tab[]) {
    int count = 0;
    node *walk = head;
    while (walk != NULL) {
        tab[count++] = walk->val;
        walk = walk->next;
    }
    return count;
}

void display_tab(int tab[], int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");
}

int main() {
    node *head = NULL;

    head = append_val(head, 42);
    head = append_val(head, 12);
    head = append_val(head, -5);
    head = append_val(head, 41);
    
    
    int new_tab[4];
    int new_size = list2tab(head, new_tab);
    printf("Tableau à partir de la liste : ");
    display_tab(new_tab, new_size);

    
    int tab[] = {42, 12, -5, 41};
    int size = sizeof(tab) / sizeof(tab[0]);
    node *new_list = tab2list(tab, size);
    printf("Liste à partir du tableau : ");
    print_list(new_list);

    return 0;
}

